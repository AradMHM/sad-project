from django.apps import AppConfig


class MedicinePageConfig(AppConfig):
    name = 'medicine_page'
