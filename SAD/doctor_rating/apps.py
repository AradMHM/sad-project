from django.apps import AppConfig


class DoctorRatingConfig(AppConfig):
    name = 'doctor_rating'
